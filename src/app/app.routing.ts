import {Routes, RouterModule} from '@angular/router';
import {DoughnutChartComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {SolicitudesComponent} from "./solicitudes/solicitudes.component";
import {EstadoServidoresComponent} from "./estado-servidores/estado-servidores.component";
import { AdministracionCuentaComponent } from './administracion-cuenta/administracion-cuenta.component';
import { SoporteComponent } from './soporte/soporte.component';
import { UltimasSolicitudesHistoricoComponent } from './ultimas-solicitudes-historico/ultimas-solicitudes-historico.component';
import { Error404Component } from './error404/error404.component';
import { SolicitudesHistoricosComponent } from './solicitudes-historicos/solicitudes-historicos.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'home', component: DoughnutChartComponent},
  {path: 'requests', component: SolicitudesComponent},
  {path: 'server-status', component: EstadoServidoresComponent},
  {path: 'administracion-cuenta', component: AdministracionCuentaComponent},
  {path: 'error404', component: Error404Component},
  {path: 'soporte', component: SoporteComponent},
  {path: 'ultimas-solicitudes-historico', component: UltimasSolicitudesHistoricoComponent},
  {path: 'requests-historico', component: SolicitudesHistoricosComponent},
  {path: '**', redirectTo: ''}
];

export const appRoutingModule = RouterModule.forRoot(routes);
