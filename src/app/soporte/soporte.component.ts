import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MetricasService } from '../services/metricas.service';

@Component({
  selector: 'app-soporte',
  templateUrl: './soporte.component.html',
  styleUrls: ['./soporte.component.css']
})
export class SoporteComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private router: Router, private metricaService: MetricasService) {
}

//Navbar Sesión
metrics: any = []
rol: any;
  ngOnInit() {
    this.rol = localStorage.getItem('session')
    this.metricaService.getAllMetricas().subscribe((data) => {
      this.metrics = data;
      console.log(this.metrics)
    })
  
  }

}
