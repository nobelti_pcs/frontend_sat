import { Component } from '@angular/core';
import { MetricasService } from '../app/services/metricas.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  //contructor para sesión
  constructor(private metricaService: MetricasService) {
  }

  date = '';
  user = '';
  title = 'SAT';
  
 /* metrics: any = []
  rol: any;
  ngOnInit() {
    this.rol = localStorage.getItem('session')
    this.metricaService.getAllMetricas().subscribe((data) => {
      this.metrics = data;
      console.log(this.metrics)
    })
    localStorage.clear();
  }
  */
}

