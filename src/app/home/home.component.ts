import { Component, Input } from '@angular/core';
import { ChartData, ChartType } from 'chart.js';
import { Router, ActivatedRoute } from '@angular/router';
import { MetricasService } from '../services/metricas.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class DoughnutChartComponent {

  //Chart de anillo
  public doughnutChartLabels: string[] = ['Running', 'Shutdown'];
  public doughnutChartData: ChartData<'doughnut'> = {
    labels: this.doughnutChartLabels,
    datasets: [
      { data: [0, 0, 0, 0, 0, 0, 0, 8] },
    ]
  };
  
  public doughnutChartType: ChartType = 'doughnut';

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  //chart de barra
  constructor(private metricaService: MetricasService, private route: Router) {
  }

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartType: ChartType = 'bar';
  public barChartLabels = ['Porcentaje %'];
  public barChartLegend = true;
  public barChartData = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'CPU' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'RAM' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'HDD' }
  ];

  //Botones Dashboard
  exitososHistorico: any = 0;
  serviciosHistorico: any = 0;
  erroresHistorico: any = 0;
  metricsHistorico: any = [];
  exitosos: any = 0;
  servicios: any = 0;
  errores: any = 0;
  metrics: any = [];
  rol: any;
  historico: any;
  ngOnInit() {
    this.rol = localStorage.getItem('session')
    this.metricaService.getAllMetricas().subscribe((data) => {
      this.metrics = data;
      console.log(this.metrics)
      this.exitosos = this.metrics.status.exitosos;
      this.errores = this.metrics.status.errores;
      this.servicios = this.exitosos + this.errores;
    });

    this.metricaService.getAllMetricasHist().subscribe((data) => {
      this.metricsHistorico = data;
      this.exitososHistorico = this.metricsHistorico.status.exitosos;
      this.erroresHistorico = this.metricsHistorico.status.errores;
      this.serviciosHistorico = this.exitososHistorico + this.erroresHistorico;
    });

  }

  setEnv(){
    this.historico= false;
    localStorage.setItem("historico", this.historico);
  }


}

