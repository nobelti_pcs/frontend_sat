export interface Solicitud {
  proyectoEsb: string;
  invocaciones: number;
  exitosos: number;
  errores: number;
  error: string;
}
