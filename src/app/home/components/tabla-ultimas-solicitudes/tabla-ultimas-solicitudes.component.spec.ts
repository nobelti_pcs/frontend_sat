import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaUltimasSolicitudesComponent } from './tabla-ultimas-solicitudes.component';

describe('TablaUltimasSolicitudesComponent', () => {
  let component: TablaUltimasSolicitudesComponent;
  let fixture: ComponentFixture<TablaUltimasSolicitudesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaUltimasSolicitudesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaUltimasSolicitudesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
