import { Component, OnInit } from '@angular/core';
import { UltimasSolicitudesService } from 'src/app/services/ultimas-solicitudes.service';

@Component({
  selector: 'app-tabla-ultimas-solicitudes',
  templateUrl: './tabla-ultimas-solicitudes.component.html',
  styleUrls: ['./tabla-ultimas-solicitudes.component.css']
})

export class TablaUltimasSolicitudesComponent implements OnInit {
  date = '08-12-2021 18:00:00';
  user = 'pdodero';

  solicitudes:any = [];

  constructor(private ultimasSolicitudes: UltimasSolicitudesService) { }

  ngOnInit(): void {
    this.ultimasSolicitudes.getAllRequest24().subscribe((data) => {
      this.solicitudes = data
    })
  }

}
