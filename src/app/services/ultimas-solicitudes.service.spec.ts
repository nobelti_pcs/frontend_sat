import { TestBed } from '@angular/core/testing';

import { UltimasSolicitudesService } from './ultimas-solicitudes.service';

describe('UltimasSolicitudesService', () => {
  let service: UltimasSolicitudesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UltimasSolicitudesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
