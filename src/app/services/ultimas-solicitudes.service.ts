import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UltimasSolicitudesService {

  constructor(private http: HttpClient ) { }

  getAllRequest() {
    return this.http.get('http://25.65.181.0:15482/api/v1/metricas/lista')
  }

  getAllRequest24() {
    return this.http.get('http://25.65.181.0:15482/api/v1/metricas/lista/24hrs');
  }
}