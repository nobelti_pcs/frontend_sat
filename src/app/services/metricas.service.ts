import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MetricasService {

  constructor(private http: HttpClient) { }

  getAllMetricas() {
    return this.http.get('http://25.65.181.0:15482/api/v1/metricas/24hrs')
  }

  getAllMetricasHist() {
    return this.http.get('http://25.65.181.0:15482/api/v1/metricas')
  }
}
