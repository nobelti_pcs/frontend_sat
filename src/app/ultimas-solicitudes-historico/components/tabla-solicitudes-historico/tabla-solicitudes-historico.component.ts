import { Component, OnInit } from '@angular/core';
import { UltimasSolicitudesService } from 'src/app/services/ultimas-solicitudes.service';

@Component({
  selector: 'app-tabla-solicitudes-historico',
  templateUrl: './tabla-solicitudes-historico.component.html',
  styleUrls: ['./tabla-solicitudes-historico.component.css']
})
export class TablaSolicitudesHistoricoComponent implements OnInit {

  date = '08-12-2021 18:00:00';
  user = 'pdodero';

  solicitudes:any = [];

  constructor(private ultimasSolicitudes: UltimasSolicitudesService) { }

  ngOnInit(): void {
    this.ultimasSolicitudes.getAllRequest().subscribe((data) => {
      this.solicitudes = data
    })
  } 

}
