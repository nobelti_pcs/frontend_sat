import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaSolicitudesHistoricoComponent } from './tabla-solicitudes-historico.component';

describe('TablaSolicitudesHistoricoComponent', () => {
  let component: TablaSolicitudesHistoricoComponent;
  let fixture: ComponentFixture<TablaSolicitudesHistoricoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaSolicitudesHistoricoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaSolicitudesHistoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
