import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UltimasSolicitudesHistoricoComponent } from './ultimas-solicitudes-historico.component';

describe('UltimasSolicitudesHistoricoComponent', () => {
  let component: UltimasSolicitudesHistoricoComponent;
  let fixture: ComponentFixture<UltimasSolicitudesHistoricoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UltimasSolicitudesHistoricoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UltimasSolicitudesHistoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
