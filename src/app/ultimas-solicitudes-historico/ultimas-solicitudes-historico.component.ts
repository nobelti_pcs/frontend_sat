import { Component, OnInit } from "@angular/core";
import { ChartData, ChartType } from "chart.js";
import { MetricasService } from "../services/metricas.service";

@Component({
  selector: "app-ultimas-solicitudes-historico",
  templateUrl: "./ultimas-solicitudes-historico.component.html",
  styleUrls: ["./ultimas-solicitudes-historico.component.css"],
})
export class UltimasSolicitudesHistoricoComponent {
  constructor(private metricaService: MetricasService) {}

  //Chart de anillo
  public doughnutChartLabels: string[] = ["Running", "Shutdown"];
  public doughnutChartData: ChartData<"doughnut"> = {
    labels: this.doughnutChartLabels,
    datasets: [{ data: [0, 0, 0, 0, 0, 0, 0, 8] }],
  };

  public doughnutChartType: ChartType = "doughnut";

  // events
  public chartClicked({
    event,
    active,
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {
    console.log(event, active);
  }

  public chartHovered({
    event,
    active,
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {
    console.log(event, active);
  }

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
  };
  public barChartType: ChartType = "bar";
  public barChartLabels = ["Porcentaje %"];
  public barChartLegend = true;
  public barChartData = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: "CPU" },
    { data: [28, 48, 40, 19, 86, 27, 90], label: "RAM" },
    { data: [28, 48, 40, 19, 86, 27, 90], label: "HDD" },
  ];

  //Botones Dashboard
  exitosos24: any = 0;
  errores24: any = 0;
  totales24: any = 0;
  metrics24: any = [];
  exitosos: any = 0;
  servicios: any = 0;
  errores: any = 0;
  metrics: any = [];
  rol: any;
  historico: any;
  ngOnInit() {
    this.rol = localStorage.getItem("session");
    this.metricaService.getAllMetricasHist().subscribe((data) => {
      this.metrics = data;
      this.exitosos = this.metrics.status.exitosos;
      this.errores = this.metrics.status.errores;
      this.servicios = this.exitosos + this.errores;
    });
    this.metricaService.getAllMetricas().subscribe((data) => {
      this.metrics24= data;
      this.exitosos24 = this.metrics24.status.exitosos;
      this.errores24 = this.metrics24.status.errores;
      this.totales24 = this.errores24 + this.exitosos24;
    });
  }

  writeEnv(){
    this.historico = true;
    localStorage.setItem("historico", this.historico );
    console.log(this.historico)
  }
}
