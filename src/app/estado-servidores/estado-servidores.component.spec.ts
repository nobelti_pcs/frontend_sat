import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoServidoresComponent } from './estado-servidores.component';

describe('EstadoServidoresComponent', () => {
  let component: EstadoServidoresComponent;
  let fixture: ComponentFixture<EstadoServidoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstadoServidoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoServidoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
