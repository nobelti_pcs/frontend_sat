export interface Server {
  arch: string;
  vendorId: string;
  status: string;
  ipAddress: string;
  so: string;
  type: string;
  vendor: string;
  cpu: string;
  cores: number;
  ram: string;
  storage: string;
}
