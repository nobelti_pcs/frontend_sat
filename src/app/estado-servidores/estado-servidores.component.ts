import {Component, OnInit} from '@angular/core';
import {Server} from "./server.interface";
import {ChartData, ChartType} from "chart.js";
import { MetricasService } from '../services/metricas.service';

@Component({
  selector: 'app-estado-servidores',
  templateUrl: './estado-servidores.component.html',
  styleUrls: ['./estado-servidores.component.css']
})
export class EstadoServidoresComponent implements OnInit {
  user = 'pdodero';

  server: Server = {
    arch: 'x86_64',
    vendorId: 'GenuineIntel',
    status: 'running',
    ipAddress: '127.0.0.1',
    so: 'Linux',
    type: 'Server',
    vendor: 'VMware',
    cpu: 'Intel(R) Xeon(R) CPU E5-2697 v2 @ 2.70GHz',
    cores: 4,
    ram: '32GB',
    storage: '500GB'
  };

  doughnutChartLabels: string[] = ['Running', 'Shutdown'];
  doughnutChartData: ChartData<'doughnut'> = {
    labels: this.doughnutChartLabels,
    datasets: [
      {data: [1, 7]},
    ]
  };

  doughnutChartType: ChartType = 'doughnut';

  //Navbar Sesión
  constructor(private metricaService: MetricasService) {
  }

  metrics: any = []
  rol: any;
  ngOnInit() {
    this.rol = localStorage.getItem('session')
    this.metricaService.getAllMetricas().subscribe((data) => {
      this.metrics = data;
      console.log(this.metrics)
    })

  }
}
