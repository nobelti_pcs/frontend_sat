import {NgModule} from '@angular/core';
import {DoughnutChartComponent} from './home/home.component';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import {NgChartsModule} from 'ng2-charts';
import {appRoutingModule} from './app.routing';
import { NavbarComponent } from './navbar/navbar.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { EstadoServidoresComponent } from './estado-servidores/estado-servidores.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginService } from './login/services/login.service';
import { TrackTableService } from './solicitudes/services/track-table.service';
import { HttpClientModule } from '@angular/common/http';
import { TablaSolicitudesHistoricoComponent } from './ultimas-solicitudes-historico/components/tabla-solicitudes-historico/tabla-solicitudes-historico.component';
import { TablaUltimasSolicitudesComponent } from './home/components/tabla-ultimas-solicitudes/tabla-ultimas-solicitudes.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UltimasSolicitudesHistoricoComponent } from './ultimas-solicitudes-historico/ultimas-solicitudes-historico.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarUserComponent } from './navbar-user/navbar-user.component';
import { Error404Component } from './error404/error404.component';
import { SoporteComponent } from './soporte/soporte.component';
import { AdministracionCuentaComponent } from './administracion-cuenta/administracion-cuenta.component';
import { SolicitudesHistoricosComponent } from './solicitudes-historicos/solicitudes-historicos.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DoughnutChartComponent,
    NavbarComponent,
    SolicitudesComponent,
    EstadoServidoresComponent,
    TablaUltimasSolicitudesComponent,
    UltimasSolicitudesHistoricoComponent,
    FooterComponent,
    TablaSolicitudesHistoricoComponent,
    NavbarUserComponent,
    Error404Component,
    SoporteComponent,
    AdministracionCuentaComponent,
    SolicitudesHistoricosComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    NgChartsModule,
    appRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [LoginService, TrackTableService],
  bootstrap: [AppComponent]
})
export class AppModule {
}