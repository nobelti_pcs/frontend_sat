import { Component, OnInit } from "@angular/core";
import { Solicitud } from "./request.interface";
import { TrackTableService } from "src/app/solicitudes/services/track-table.service";
import {
  faFilePdf,
  faMagnifyingGlassPlus,
  faEnvelope,
  faFileExcel,
} from "@fortawesome/free-solid-svg-icons";
import jsPDF from "jspdf";
import { MetricasService } from "../services/metricas.service";
import html2canvas from "html2canvas";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  FormBuilder,
  FormArray,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import * as moment from "moment";
import Swal from "sweetalert2";
import * as XLSX from "xlsx";
// @ts-ignore
import * as formatXML from "xml-formatter";
import { local } from "d3-selection";

@Component({
  selector: "app-solicitudes",
  templateUrl: "./solicitudes.component.html",
  styleUrls: ["./solicitudes.component.css"],
  providers: [TrackTableService],
})
export class SolicitudesComponent implements OnInit {
  form: FormGroup;
  formCorreo = new FormGroup({
    asunto: new FormControl("", [Validators.required]),
  });
  formFilter = new FormGroup({
    request: new FormControl({ value: "", disabled: true }, [
      Validators.required,
    ]),
    response: new FormControl({ value: "", disabled: true }, [
      Validators.required,
    ]),
    fecha: new FormControl("", [Validators.required]),
    fechaDesde: new FormControl("", [Validators.required]),
    key: new FormControl("", [Validators.required]),
  });
  faFilePdf = faFilePdf;
  faFileExcel = faFileExcel;
  faMagnifyingGlassPlus = faMagnifyingGlassPlus;
  faEnvelope = faEnvelope;
  correEnviar: any = "";
  subject: any = "";
  bodyEmail: any = "";
  date = "08-12-2021 18:00:00";
  user = "pdodero";
  pdf: any;
  solicitudes: any = [];
  projects: any = [];
  operations: any = [];
  emails: any = [];
  projectSelected: any = "";
  operSelected: any = "";
  detalleSolicitud: any = [];
  detalleSolicitudInput: any = [];
  detalleSolicitudOutput: any = [];
  detalleSolicitudFault: any = [];
  detalleOperacion: any = [];
  detalleProyecto: any = [];
  disableOperations = true;
  disableButtonPdf = false;
  disableFilters = true;
  mensajeCustom: any = "";
  fechaNormal: boolean = false;
  fechaDesde: boolean = false;
  operFecha: boolean = true;
  operFecha1: boolean = false;
  operFecha2: boolean = false;
  faultValid: boolean = false;
  detalleTraza: any = [];
  pdfBase: any = "";
  id: any;
  userEmpresa: any;
  dataFields: any = "";
  empresa: any = "";

  constructor(
    private fb: FormBuilder,
    private trackTableService: TrackTableService,
    private metricaService: MetricasService,
    private modal: NgbModal
  ) {
    this.form = this.fb.group({
      checkArray: this.fb.array([], [Validators.required]),
    });
  }

  getId(id) {
    console.log("id que llega", id);
    console.log("variable de clase inicial", this.id);
    this.id = id;
    console.log("variable de clase final", this.id);
    return id;
  }

  onCheckboxChange(e: any) {
    const checkArray: FormArray = this.form.get("checkArray") as FormArray;
    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: any) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  submitForm() {
    const dest = this.form.value;
    let emails = "";
    for (let c in dest) {
      emails = dest[c] + ",";
    }
    let emailApi = emails.substring(0, emails.length - 1);
    console.log(emailApi);
    this.sendMailPdf(emailApi, this.id);
  }

  metrics: any = [];
  rol: any;

  ngOnInit(): void {
    this.trackTableService.getProjects().subscribe((data1) => {
      this.projects = data1;
    });
    this.trackTableService
      .getUserById(localStorage.getItem("idUser"))
      .subscribe((data2) => {
        this.userEmpresa = data2;
        this.empresa= this.userEmpresa.idEmpresa;
      });


    this.trackTableService
      .getAllTrackingCurrentDay(this.empresa)
      .subscribe((data) => {
        this.solicitudes = data;
      });

    //Navbar sesión
    this.rol = localStorage.getItem("session");
    this.metricaService.getAllMetricas().subscribe((data) => {
      this.metrics = data;
      console.log(this.metrics);
    });
  }

  getOperationsByProject() {
    this.trackTableService.getOperations().subscribe((data2) => {
      let tmp: any = data2;
      this.operations = [];
      tmp.map((tm: any) => {
        if (tm.idProyecto == this.projectSelected) {
          this.operations.push(tm);
        }
      });
      console.log(this.operations);
    });
  }
  filter() {
    let request = this.formFilter.get("request")?.value;
    let response = this.formFilter.get("response")?.value;
    let key = this.formFilter.get("key")?.value;
    let fecha = this.formFilter.get("fecha")?.value;
    let empresa = localStorage.getItem("idEmpresa");

    if (fecha != null) {
      if (fecha.length == 0 || fecha == null) {
        fecha = null;
      }
    }
    let fechaDesde = this.formFilter.get("fechaDesde")?.value;
    if (fechaDesde != null) {
      if (fechaDesde.length == 0 || fechaDesde == null) {
        fechaDesde = null;
      }
    }

    this.trackTableService
      .getAllByFilters(
        this.operSelected,
        this.projectSelected,
        request,
        response,
        fecha,
        fechaDesde,
        key,
        empresa
      )
      .subscribe((res) => {
        this.solicitudes = res;
      });
  }

  clear() {
    this.operFecha1 = false;
    this.operFecha2 = false;
    this.operFecha = true;
    this.fechaNormal = false;
    this.fechaDesde = false;
    this.formFilter.controls["fecha"].setValue(null);
    this.formFilter.controls["fechaDesde"].setValue(null);
    this.formFilter.controls["request"].setValue(null);
    this.formFilter.controls["response"].setValue(null);
    this.formFilter.controls["key"].setValue(null);
    this.projectSelected = null;
    this.operSelected = null;
    this.formFilter.get("request").disable();
    this.formFilter.get("response").disable();
    this.disableOperations = true;
    //Vaciar tabla
    for (let i = this.solicitudes.length; i > 0; i--) {
      this.solicitudes.pop();
    }
  }

  onChangeProj(e: any) {
    this.operations = [];
    this.projectSelected = e.target.value;
    this.getOperationsByProject();
    this.disableOperations = false;
    this.formFilter.get("request").enable();
    this.formFilter.get("response").enable();
  }
  onChangeOper(e: any) {
    this.operSelected = e.target.value;
  }

  onChangeDateSelector(e: any) {
    console.log(e.target.value);
    if (e.target.value == 0) {
      this.operFecha = true;
      this.operFecha1 = false;
      this.operFecha2 = false;
      this.fechaNormal = false;
      this.fechaDesde = false;
      this.formFilter.controls["fecha"].setValue(null);
      this.formFilter.controls["fechaDesde"].setValue(null);
    }
    if (e.target.value == 1) {
      this.fechaNormal = true;
      this.fechaDesde = false;
      this.operFecha = false;
      this.operFecha1 = true;
      this.operFecha2 = false;
      this.formFilter.controls["fecha"].setValue(null);
      this.formFilter.controls["fechaDesde"].setValue(null);
    }
    if (e.target.value == 2) {
      this.fechaDesde = true;
      this.fechaNormal = false;
      this.operFecha = false;
      this.operFecha1 = false;
      this.operFecha2 = true;
      this.formFilter.controls["fecha"].setValue(null);
      this.formFilter.controls["fechaDesde"].setValue(null);
    }
  }

  onChangeEmail(mailValue: any) {
    this.correEnviar = mailValue;
  }

  sendEmail() {
    let destinatarios = [];
    let asunto = this.formCorreo.get("asunto")?.value;
    this.trackTableService
      .sendEmail(this.correEnviar, asunto, this.mensajeCustom)
      .subscribe((resp) => {
        Swal.fire("OK", "Se ha enviado el correo", "success");
        console.log(resp);
      });
  }

  getBodyEmail(pname, oname, date) {
    let body =
      "Se ha solicitado información de la petición del proyecto: " +
      pname +
      " operación: " +
      oname +
      " la cuál fue realizada a la fecha: " +
      date +
      " para más información revisar PDF adjunto.";

    return body;
  }

  sendMailPdf(dest, id) {
    this.trackTableService.getTrackingById(id).subscribe((res) => {
      try {
        this.dataFields = res;
        let projectName = this.dataFields.nombreIntegracion;
        let operationName = this.dataFields.nombreOperacion;
        let date = this.dataFields.fechaCreacion;
        let subject = "";
        subject = "Informacion de traza " + projectName + " " + operationName;
        this.trackTableService.getPDF(res).subscribe((response) => {
          this.trackTableService
            .sendMailPdf(
              response,
              this.getBodyEmail(projectName, operationName, date),
              dest,
              subject
            )
            .subscribe((data) => {
              Swal.fire({
                position: "center",
                icon: "success",
                title: "Se envió tu detalle a los correos seleccionados.",
                showConfirmButton: false,
                timer: 1500,
              });
            });
        });
      } catch (error) {
        console.error(error);
      }
    });
  }

  getPdf(id) {
    console.log(id);
    this.disableButtonPdf = true;
    this.trackTableService.getTrackingById(id).subscribe((res) => {
      this.detalleTraza = res;
      try {
        this.trackTableService
          .getPDF(this.detalleTraza)
          .subscribe((response) => {
            console.log("url---", response);
            this.printPdf(response);
            this.disableButtonPdf = false;
            Swal.fire({
              position: "center",
              icon: "success",
              title: "Se descargo tu detalle en pdf.",
              showConfirmButton: false,
              timer: 1500,
            });
          });
      } catch (err) {
        console.error(err.message);
        this.disableButtonPdf = false;
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Ha ocurrido un error",
          footer: '<a href="">Contactar a soporte</a>',
        });
      }
    });
  }

  printPdf(body) {
    const linkSource = `data:application/pdf;base64,${body}`;
    const downloadLink = document.createElement("a");
    const fileName = "abc.pdf";
    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }

  downloadPdf(): void {
    const data: any = document.querySelector(
      "body > ngb-modal-window > div > div > div.exports"
    );
    const doc = new jsPDF("p", "pt", "a4");
    const options = {
      background: "white",
      scale: 3,
    };
    html2canvas(data, options)
      .then((canvas) => {
        console.log(data);
        const img = canvas.toDataURL("image/PNG");
        const bufferX = 15;
        const bufferY = 15;
        const imgProps = (doc as any).getImageProperties(img);
        const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
        const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;

        doc.addImage(
          img,
          "PNG",
          bufferX,
          bufferY,
          pdfWidth,
          pdfHeight,
          undefined,
          "FAST"
        );
        return doc;
      })
      .then((docResult) => {
        docResult.save(`${new Date().toISOString()}_report.pdf`);
      })
      .catch((err) => {
        console.error(err);
      });
  }

  downloadPdfMailing(): void {
    const data: any = document.querySelector(
      "body > ngb-modal-window > div > div > div.exports"
    );
    const doc = new jsPDF("p", "pt", "a4");
    const options = {
      background: "white",
      scale: 3,
    };
    html2canvas(data, options).then((canvas) => {
      console.log(data);
      const img = canvas.toDataURL("image/PNG");
      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(
        img,
        "PNG",
        bufferX,
        bufferY,
        pdfWidth,
        pdfHeight,
        undefined,
        "FAST"
      );
      let tt = btoa(doc.output());
      this.pdf = tt;
      return tt;
    });
  }

  openSM(contenido) {
    this.modal.open(contenido, { size: "xl", backdrop: false });
    console.log("contenido", contenido);
  }

  closeModal() {
    this.modal.dismissAll();
  }

  getEmails() {
    this.trackTableService.getEmails().subscribe((data) => {
      this.emails = data;
      console.log(data);
    });
  }

  exportTableToExcel() {
    let element = document.getElementById("table_request");
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");

    XLSX.writeFile(wb, "ExcelSheet.xlsx");
  }

  /** FORMAT FUNCTIONS  */

  formatterXML(xml) {
    try {
      return formatXML(xml);
    } catch (err) {
      console.log("ERROR -----> ", err);
      return xml;
    }
  }

  formatJson(json) {
    try {
      let obj = JSON.parse(json);
      return JSON.stringify(obj, null, "\t");
    } catch (err) {
      return json;
    }
  }

  getDetalleRequest(id: number) {
    this.trackTableService
      .getBodyResponse(id, "response")
      .subscribe((response: string) => {
        response.charAt(0) === "{"
          ? (this.detalleSolicitudOutput = this.formatJson(response))
          : (this.detalleSolicitudOutput = this.formatterXML(response));

        this.trackTableService
          .getBodyResponse(id, "request")
          .subscribe((request: string) => {
            request.charAt(0) === "{"
              ? (this.detalleSolicitudInput = this.formatJson(request))
              : (this.detalleSolicitudInput = this.formatterXML(request));

            this.trackTableService
              .getBodyResponse(id, "fault")
              .subscribe((fault: string) => {
                if (fault != null && fault != "") {
                  this.faultValid = true;
                  fault.charAt(0) === "{"
                    ? (this.detalleSolicitudFault = this.formatJson(fault))
                    : (this.detalleSolicitudFault = this.formatterXML(fault));
                }
              });
          });
      });
    this.trackTableService.getTrackingById(id).subscribe((data) => {
      this.detalleSolicitud = data;
      this.trackTableService
        .getOperationById(this.detalleSolicitud.idOperacion)
        .subscribe((dt) => {
          this.detalleOperacion = dt;
          this.trackTableService
            .getProjectById(this.detalleOperacion.idProyecto)
            .subscribe((tmp) => {
              this.detalleProyecto = tmp;
              const input = formatXml(this.detalleSolicitud.input, "\t").trim();
              console.log(input);
              const output = formatXml(
                this.detalleSolicitud.output,
                "\t"
              ).trim();
              this.mensajeCustom = `
          <p><b>Proyecto :</b>  ${this.detalleProyecto.nombreProyecto}</p>
          <p><b>Operación:</b>  ${this.detalleOperacion.nombreOperacion}</p>
          <p><b>Response Code:</b>  ${this.detalleSolicitud.httpCode}</p>
          <p><b>Fecha:</b>  ${this.detalleSolicitud.fechaCreacion}</p>
          <p><b>Hora Inicio:</b>  ${this.detalleSolicitud.hrStart}</p>
          <p><b>Hora Fin:</b>  ${this.detalleSolicitud.hrEnd}</p>
          <p>----------------------------------------------------------</p></br>
          <p><b>Input:</b> </p></br>
          <p>${input}</p>
          <p>----------------------------------------------------------</p></br>
          <p><b>Output:</b> </p></br>
          <p>${output}</p>
          <p>----------------------------------------------------------</p></br>
          <p><b>Fault:</b> </p>
          <p>${this.detalleSolicitud.fault}</p>
          <p>----------------------------------------------------------</p></br>`;
            });
        });
    });
  }
}

function formatXml(xml, tab) {
  // tab = Valor opcional para el salto de linea
  var formatted = "",
    indent = "";
  tab = tab || "\t";
  xml.split(/>\s*</).forEach(function (node) {
    if (node.match(/^\/\w/)) indent = indent.substring(tab.length);
    formatted += indent + "<" + node + ">\r\n";
    if (node.match(/^<?\w[^>]*[^\/]$/)) indent += tab;
  });
  return formatted.substring(1, formatted.length - 3);
}

function formatJson(json) {
  return JSON.stringify(json, null, "\t");
}
