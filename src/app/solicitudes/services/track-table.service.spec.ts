import { TestBed } from '@angular/core/testing';

import { TrackTableService } from './track-table.service';

describe('TrackTableService', () => {
  let service: TrackTableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrackTableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
