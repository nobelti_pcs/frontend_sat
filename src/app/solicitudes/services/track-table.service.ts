import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { catchError } from "rxjs/operators";
import { Observable, throwError } from "rxjs";

const urlBase: string = "http://25.65.181.0";
const port: string = ":15480";
const url: string = urlBase + port;

@Injectable({
  providedIn: "root",
})
export class TrackTableService {
  constructor(private http: HttpClient) {}

  getPDF(body) {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",
      }),
      responseType: "text",
    };
    console.log("bodyyyy", body);
    return this.http.post(
      "http://25.65.181.0:15484/api/v1/pdf/",
      body,
      httpOptions
    );
  }

  getAllTracking(idEmpresa) {
    if(idEmpresa==null){idEmpresa=1}
    const httpOptions: Object = {
      headers: new HttpHeaders({
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",

        idEmpresa: idEmpresa.toString(),
      }),
      responseType: "json",
    };
    return this.http.get(url + "/api/v1/traza", httpOptions);
  }

  getAllTrackingCurrentDay(idEmpresa) {
    if(idEmpresa==null || idEmpresa==''){idEmpresa=1}
    const date = new Date();
    const anio = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const formatedDate = anio + "-" + month + "-" + day;

    console.log("idddd", idEmpresa);

    const httpOptions: Object = {
      headers: new HttpHeaders({
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",

        idEmpresa: idEmpresa.toString(),
      }),
      responseType: "json",
    };

    return this.http.get(
      url + "/api/v1/traza?fecha=" + formatedDate,
      httpOptions
    );
  }

  sendMailPdf(pdf, body: string, destinatario: string, asunto: string) {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",
        attachment: pdf,
      }),
      responseType: "text",
    };
    return this.http.post(
      `http://25.65.181.0:15483/api/v1/sendemail?destinatarios=${destinatario}&asunto=${asunto}`,
      body,
      httpOptions
    );
  }

  getTrackingById(id: number) {
    return this.http.get(url + "/api/v1/traza/" + id);
  }

  getOperationById(id: number) {
    return this.http.get(url + "/api/v1/operaciones/" + id);
  }

  getProjectById(id: number) {
    return this.http.get(url + "/api/v1/proyectos/" + id);
  }

  getEmails() {
    return this.http.get(url + "/api/v1/libretacorreos");
  }

  getProjects() {
    return this.http.get(url + "/api/v1/proyectos");
  }
  getOperations() {
    return this.http.get(url + "/api/v1/operaciones");
  }

  getAllByFilters(
    idOpeacion,
    idProyecto,
    request: string,
    response: string,
    fecha: string,
    fechaDesde: string,
    key: string,
    idEmpresa
  ) {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",
        idEmpresa: idEmpresa.toString(),
      }),
      responseType: "json",
    };
    if (idProyecto == null) {
      idProyecto = "";
    }
    if (idOpeacion == null) {
      idOpeacion = "";
    }
    if (request == null) {
      request = "";
    }
    if (response == null) {
      response = "";
    }
    if (key == null) {
      key = "";
    }
    console.log("fecha------>", fecha);
    console.log("fechaDesde------>", fechaDesde);
    if (fecha != null || fechaDesde != null) {
      if (fecha == null && fechaDesde != null) {
        return this.http.get(
          `http://25.65.181.0:15480/api/v1/traza?idOperacion=${idOpeacion}&idProyecto=${idProyecto}&request=${request}&response=${response}&key=${key}&fechaDesde=${fechaDesde}`,
          httpOptions
        );
      } else {
        console.log("Entre con fecha!");
        return this.http.get(
          `http://25.65.181.0:15480/api/v1/traza?idOperacion=${idOpeacion}&idProyecto=${idProyecto}&request=${request}&response=${response}&key=${key}&fecha=${fecha}`,
          httpOptions
        );
      }
    } else {
      return this.http.get(
        `http://25.65.181.0:15480/api/v1/traza?idOperacion=${idOpeacion}&idProyecto=${idProyecto}&request=${request}&response=${response}&key=${key}`,
        httpOptions
      );
    }
  }

  getUserById(id) {
    return this.http.get(url + "/api/v1/usuarios/" + id);
  }

  getBodyResponse(idRegistro: number, tipo: string) {
    const httpOptions: Object = {
      headers: new HttpHeaders({
        Accept: "text/html",
        "Content-Type": "text/plain; charset=utf-8",
      }),
      responseType: "text",
    };
    return this.http.get(
      `http://25.65.181.0:15480/api/v1/traza/${idRegistro}/body?tipo=${tipo}`,
      httpOptions
    );
  }

  sendEmail(
    destinatario: string,
    asunto: string,
    mensaje: string
  ): Observable<any> {
    return this.http
      .post(
        "http://25.65.181.0:15483/api/v1/sendemail?destinatarios=" +
          destinatario +
          "&asunto=" +
          asunto,
        mensaje
      )
      .pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error("An error occurred:", error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return throwError("Something bad happened; please try again later.");
  }
}
