export interface Solicitud {
  id: string;
  integracion: string;
  operacion: string;
  estado: number;
  creacion: string;
  ipCliente: string;
  request: string;
  detalle: string;
}
