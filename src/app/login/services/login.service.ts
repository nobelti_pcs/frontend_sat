import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }


  create(name: string, pass: string): Observable<any> {
    try {
    return this.http.post('http://25.65.181.0:15481/api/v1/login', {
      'usuario': name,
      'password': btoa(pass)
    }).pipe(
      catchError((error) => {
        if (error.error instanceof ErrorEvent) {
          console.error('An error occurred:', error.error.message);
        } else {
          console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
          return error.status;
        }
        return throwError('Something bad happened; please try again later.');
      })
      //catchError(this.handleError)
    );}
    catch(err) {
      return err;
    }
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  };

  /*login(name:string, pass:string) {
    this.http.post('http://25.65.181.0:15481/api/v1/login',{
      'usuario': name,
      'password': pass
    })
  }*/
}
