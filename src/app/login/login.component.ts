import {Component, OnInit,  EventEmitter, Output} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from './services/login.service';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  formLogin = new FormGroup({
    user: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  @Output() rol = new EventEmitter<string>();//new

  constructor(private route: ActivatedRoute,
              private router: Router, 
              private loginService: LoginService,
              private http:HttpClient) {
  }

  ngOnInit(): void {
    localStorage.clear();
  }

  onClickLogin() {
    let user = this.formLogin.get('user')?.value;
    let password = this.formLogin.get('password')?.value;
      
    //this.loginService.create('user-demo','123abc').subscribe((data) => {
    this.loginService.create(user, password).subscribe((dt) => {
      //console.log(data);
      if (dt.codigo == 0) {
          //this.idRol = data.idRol;
          this.rol.emit(dt.rol); //new
          //this.router.navigate(['/home'], { data: dt.rol }); 
          localStorage.setItem('idUser',dt.idUsuario);
          localStorage.setItem('session', dt.rol)
          this.router.navigateByUrl('/home', {state: {data: dt.rol}})
      }
      else{
        Swal.fire( "Oops" ,"Credenciales incorrectas" , "error" );
      }
    })
  }



}
