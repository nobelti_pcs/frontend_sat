import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesHistoricosComponent } from './solicitudes-historicos.component';

describe('SolicitudesHistoricosComponent', () => {
  let component: SolicitudesHistoricosComponent;
  let fixture: ComponentFixture<SolicitudesHistoricosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitudesHistoricosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesHistoricosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
