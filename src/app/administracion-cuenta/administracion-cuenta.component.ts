import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { MetricasService } from "../services/metricas.service";
import Swal from "sweetalert2";
import { UsersService } from "./services/users.service";
import { faUserXmark, faUserPen } from "@fortawesome/free-solid-svg-icons";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  FormBuilder,
  FormArray,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";

@Component({
  selector: "app-administracion-cuenta",
  templateUrl: "./administracion-cuenta.component.html",
  styleUrls: ["./administracion-cuenta.component.css"],
  providers: [UsersService],
})
export class AdministracionCuentaComponent implements OnInit {
  form: FormGroup;
  formCreateUser = new FormGroup({
    nomUser: new FormControl("", [Validators.required]),
    pwd: new FormControl("", [Validators.required]),
    email: new FormControl("", [Validators.required]),
    nombre: new FormControl("", [Validators.required]),
    apellido: new FormControl("", [Validators.required]),
    rol: new FormControl("", [Validators.required]),
  });
  formUser = new FormGroup({
    nomUser: new FormControl("", [Validators.required]),
    pwd: new FormControl("", [Validators.required]),
    email: new FormControl("", [Validators.required]),
    nombre: new FormControl("", [Validators.required]),
    apellido: new FormControl("", [Validators.required]),
    rol: new FormControl("", [Validators.required]),
  });
  constructor(
    private router: Router,
    private metricaService: MetricasService,
    private userService: UsersService,
    private modal: NgbModal
  ) {}

  //Navbar Sesión
  metrics: any = [];
  rol: any;

  faUserPen = faUserPen;
  faUserXmark = faUserXmark;
  users: any = [];
  empresas: any = [];
  empresaSelectedBuscar: any = "";
  estadoUser: any = [];
  roles: any = [];
  detalleUser: any = [];
  detalleEditUser: any = [];
  id: number = 0;
  empresaSelectedCreate: any = 1;
  rolSelectedCreate: any = 101;
  estadoCreate: any = 1;
  userEstado: any = [];
  empresaSelectedEdit: any =1;
  rolSelectedEdit: any = 101;
  estadoEdit: any = 1;

  ngOnInit() {
    this.userService.getUsers().subscribe((usr) => {
      this.users = usr;
    });
    this.userService.getEmpresas().subscribe((emp) => {
      this.empresas = emp;
    });
    this.userService.getRoles().subscribe((rol) => {
      this.roles = rol;
    });
    this.userService.getEstados().subscribe((state) => {
      this.estadoUser = state;
    });

    //Nav
    this.rol = localStorage.getItem("session");
    this.metricaService.getAllMetricas().subscribe((data) => {
      this.metrics = data;
      console.log(this.metrics);
    });
  }

  onClickState(e: any) {
    this.userService.getUserById(this.id).subscribe((data) => {
      this.userEstado = data;
      if (this.userEstado.idEstado == 1) {
        this.userEstado.idEstado = 2;
      } else {
        this.userEstado.idEstado = 1;
      }
      this.userService.updateUser(this.userEstado).subscribe((est) => {});
    });
  }

  onChangeEstadoEdit(e: any){
    this.estadoEdit = e.target.value;
  }
  onChangeRolEdit(e: any){
    this.rolSelectedEdit = e.target.value;
  }
  onChangeEmpresaBuscar(e: any) {
    this.empresaSelectedBuscar = e.target.value;
  }
  onChangeEmpresaCreate(e: any) {
    this.empresaSelectedCreate = e.target.value;
  }

  onChangeEmpresaEdit(e: any){
    this.empresaSelectedEdit = e.target.value;
  }

  onChangeRolCreate(e: any) {
    this.rolSelectedCreate = e.target.value;
  }

  onChangeEstadoCreate(e: any) {
    this.estadoCreate = e.target.value;
  }

  filterUsers() {
    let id = this.empresaSelectedBuscar;
    if (id == "Todos" || id == "") {
      this.userService.getUsers().subscribe((data) => {
        this.users = data;
      });
    } else {
      this.userService.getUsersByEmpresa(id).subscribe((us) => {
        this.users = us;
      });
    }
  }

  getId(id) {
    console.log("iddd", id);
    this.id = id;
    return id;
  }

  resetCmb(){
    this.empresaSelectedEdit=1;
    this.rolSelectedEdit = 101;
    this.estadoEdit = 1;
  }

  getDetalleUser(id) {
    this.id = id;
    this.userService.getUserById(id).subscribe((data) => {
      this.detalleUser = data;
    });
  }

  openSM(contenido) {
    this.modal.open(contenido, { size: "xl", backdrop: false });
    console.log("contenido", contenido);
  }

  closeModal() {
    this.modal.dismissAll();
  }

  cleanForm(){
    this.formCreateUser.controls["nombre"].setValue(null);
    this.formCreateUser.controls["nomUser"].setValue(null);
    this.formCreateUser.controls["pwd"].setValue(null);
    this.formCreateUser.controls["email"].setValue(null);
    this.formCreateUser.controls["apellido"].setValue(null);
  }

  validateCreateForm(){
    let nombre = this.formCreateUser.get("nombre")?.value;
    let nomUser = this.formCreateUser.get("nomUser")?.value;
    let pwd = this.formCreateUser.get("pwd")?.value;
    let email = this.formCreateUser.get("email")?.value;
    let apellido = this.formCreateUser.get("apellido")?.value;

    console.log('NOMBRE---->', nombre);

    if (nombre != null && nombre.length > 0 && nombre != ''){
      console.log('Entreeee---->');
    }else {
     alert('TIENES QUE LLENAR LOS DATOS XDDDDDDDD')
    }
       
  }

  formToUser(user) {
    let nomUser = this.formUser.get("nomUser")?.value;
    let pwd = this.formUser.get("pwd")?.value;
    let email = this.formUser.get("email")?.value;
    let nombre = this.formUser.get("nombre")?.value;
    let apellido = this.formUser.get("apellido")?.value;

    if (nomUser != null && nomUser.length > 0) {
      user.usuario = nomUser;
    }
    if (pwd != null && pwd.length > 0) {
      user.password = pwd;
    }
    if (email != null && email.length > 0) {
      user.email = email;
    }
    if (nombre != null && nombre.length > 0) {
      user.nombre = nombre;
    }
    if (apellido != null && apellido.length > 0) {
      user.apellido = apellido;
    }
    user.idRol = this.rolSelectedEdit;
    user.idEmpresa = this.empresaSelectedEdit;
    user.idEstado = this.estadoEdit;

    return user;
  }

  updateUser() {
    try {
      let editUser = this.formToUser(this.detalleUser);
      this.userService.updateUser(editUser).subscribe((data) => {
        Swal.fire({
          position: "center",
          icon: "success",
          title: "El usuario ha sido actualizado",
          showConfirmButton: false,
          timer: 1500,
        });
        this.userService.getUsers().subscribe((usr) => {
          this.users = usr;
        });
      });
    } catch (err) {
      console.error("Error: ", err.message);
    }
  }

  deleteUser(id) {
    this.userService.deleteUser(id).subscribe((data) => {
      this.userService.getUsers().subscribe((usr) => {
        this.users = usr;
      });
    });
  }

  createUserObject() {
    let nomUser = this.formCreateUser.get("nomUser")?.value;
    let pwd = this.formCreateUser.get("pwd")?.value;
    let email = this.formCreateUser.get("email")?.value;
    let nombre = this.formCreateUser.get("nombre")?.value;
    let apellido = this.formCreateUser.get("apellido")?.value;
    let user = {
      idUsuario: 0,
      idEmpresa: this.empresaSelectedCreate,
      idRol: this.rolSelectedCreate,
      idEstado: 1,
      usuario: nomUser,
      password: pwd,
      email: email,
      nombre: nombre,
      apellido: apellido,
    };
    return user;
  }

  createUser() {
    this.userService.createUser(this.createUserObject()).subscribe((data) => {
      Swal.fire({
        position: "center",
        icon: "success",
        title: "El usuario ha sido creado",
        showConfirmButton: false,
        timer: 1500,
      });
      this.userService.getUsers().subscribe((usr) => {
        this.users = usr;
      });
    });
  }

  confirmDelete(id) {
    Swal.fire({
      title: "¿Estas seguro?",
      text: "El usuario se eliminará del sistema",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si",
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteUser(id);
        Swal.fire("Eliminado!", "El usuario ha sido eliminado", "success");
      }
    });
  }
}
