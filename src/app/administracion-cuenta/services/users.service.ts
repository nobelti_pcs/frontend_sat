import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const urlBase = 'http://25.65.181.0';
const uri='/api/v1'
const port=':15480';
const url=urlBase+port+uri;

@Injectable({
  providedIn: 'root'
})

export class UsersService {

  constructor(private http: HttpClient) {}

  getUsersByEmpresa(idEmpresa){
    return this.http.get(url+'/usuarios?idEmpresa='+idEmpresa);
  }

  getUsers(){
    return this.http.get(url+'/usuarios');
  }

  getEmpresas(){
    return this.http.get(url+'/empresas')
  }

  deleteUser(id){
    return this.http.delete(url+'/usuarios/'+id);
  }

  getEstados(){
    return this.http.get(url+'/estadousuario'); 
  }

  getUserById(id){
    return this.http.get(url+'/usuarios/'+id);
  }

  updateUser(body){
    const httpOptions: Object = {
      headers: new HttpHeaders({
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",
      }),
      responseType: "json",
    };
    return this.http.post(url+'/usuarios', body, httpOptions);
  }

  getRoles(){
    return this.http.get(url+'/roles');
  }

  createUser(body){
    const httpOptions: Object = {
      headers: new HttpHeaders({
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",
      }),
      responseType: "json",
    };
    return this.http.post(url+'/usuarios', body, httpOptions);

  }

}
