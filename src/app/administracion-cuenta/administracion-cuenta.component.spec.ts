import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministracionCuentaComponent } from './administracion-cuenta.component';

describe('AdministracionCuentaComponent', () => {
  let component: AdministracionCuentaComponent;
  let fixture: ComponentFixture<AdministracionCuentaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministracionCuentaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministracionCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
